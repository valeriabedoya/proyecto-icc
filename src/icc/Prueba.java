package icc;
import java.util.Scanner;

public class Prueba {
    // Función para obtener un entero válido dentro de un rango
    public static int getInt(String mensaje, String error, int min, int max) {
        int val;
        Scanner scn = new Scanner(System.in);

        while (true) {
            System.out.println(mensaje);
            if (scn.hasNextInt()) {
                val = scn.nextInt();
                if (val < min || val > max) {
                    System.out.println(error);
                } else {
                    return val;
                }
            } else {
                scn.next();
                System.out.println(error);
            }
        }
    }

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        int numEquipos = 0;
        System.out.println("Bienvenido, este programa es un simulador de un torneo Round-Robin de volleybol.");

        // Obtener el número de equipos para la simulación
        numEquipos = getInt("¿De cuántos equipos consiste tu simulación? (2 o más): ",
                "Por favor ingresa un número válido (2 o más equipos).", 2, Integer.MAX_VALUE);

        Equipos equiposArray = new Equipos(numEquipos);

        // Solicitar nombres de equipos
        for (int i = 0; i < numEquipos; i++) {
            System.out.println("Ingresa el nombre del equipo " + (i + 1) + ": ");
            String numero = Integer.toString(i); // Número del equipo
            String nombre = scn.nextLine(); // Nombre del equipo
            equiposArray.guardarEquipo(i, numero, nombre);
        }

        int[] puntuaciones = new int[numEquipos];
        int numRondas = numEquipos - 1;

        int opcion = -1;
        while (opcion != 0) {
            // Menú de opciones
            opcion = getInt("Selecciona una opción:\n1. Consultar todas las puntuaciones\n2. Continuar con la simulacion \n0. Salir\nOpción: ",
                    "Por favor ingresa una opción válida.", 0, 2);

            switch (opcion) {
                case 1:
                    // Mostrar todas las puntuaciones
                    System.out.println("1");    
                    // Agregar lógica para consultar todas las puntuaciones.
                    break;
                case 2:
                    // Continuar con la simulación
                    System.out.println("2");
                    for (int ronda = 1; ronda <= numRondas; ronda++) {
                        System.out.println("Ronda " + ronda + ":");
                        // Agregar lógica para simular cada partido y actualizar las puntuaciones.
                        // Esto debe incluir la lógica para mostrar los resultados de cada partido.
                    }

                    System.out.println("El torneo terminó, los puntajes quedaron así:");
                    // Mostrar el resumen del torneo, incluyendo los puntajes, sets, juegos ganados y perdidos.
                    // Agregar lógica para consultar una puntuación en particular.
                    break;
                case 0:
                    System.out.println("Gracias por usar el simulador. ¡Hasta luego!");
                    break;
                default:
                    System.out.println("Opción no válida. Por favor selecciona una opción válida.");
            }
            while (/*Cuando se terminen los rounds*/) {
                // Menú de opciones después de terminar las rondas
                opcion = getInt("Selecciona una opción:\n1. Consultar todas las puntuaciones.\n 2. Consultar alguna puntuacion en particular.\n 3. Ver campeon. \n 0. Salir\nOpción: ",
                    "Por favor ingresa una opción válida.", 0, 3);
               switch (opcion) {
                case 1:
                    // Mostrar todas las puntuaciones
                    System.out.println("1");    
                    // Agregar lógica para consultar todas las puntuaciones.
                    break;
                case 2:
                    System.out.println("2");
                    // Agregar lógica para consultar una puntuación en particular.
                    break;
                case 3:
                    System.out.println("3");
                    // Agregar lógica para determinar y mostrar al campeón.
                    break;
                case 0:
                    System.out.println("Gracias por usar el simulador. ¡Hasta luego!");
                    break;
                default:
                    System.out.println("Opción no válida. Por favor selecciona una opción válida.");
            }
        }
    }
}
