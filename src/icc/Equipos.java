package icc;

public class Equipos {
        private String[][] equipos;
    
        public Equipos(int numEquipos) {
            equipos = new String[numEquipos][2];
        }
    
        public void guardarEquipo(int indice, String numero, String nombre) {
            equipos[indice][0] = numero;
            equipos[indice][1] = nombre;
        }
    
        public String obtenerNumeroEquipo(int indice) {
            return equipos[indice][0];
        }
    
        public String obtenerNombreEquipo(int indice) {
            return equipos[indice][1];
        }
}